package com.djammadev.connector;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sissoko on 14/03/2016.
 */
public class HttpProxy {


	public enum HttpMethod {
		GET, POST
	}

	private String host;
	private String path;
	private File[] files;
	private final Map<String, Object> parameters;
	private HttpURLConnection connection;

	//private static final String TAG = "HttpProxy";

	private static String boundary = "******";
	private static String lineEnd = "\r\n";
	private static String twoHyphens = "--";

	private HttpMethod method;

	private List<HttpProxyListener> listenerList;
	private String cookie;


	/**
	 * @param host
	 * @param path
	 * @param parameters
	 */
	public HttpProxy(String host, String path, Map<String, Object> parameters) {
		this.host = host;
		this.path = path;
		this.parameters = parameters;
		this.listenerList = new ArrayList<>();
	}


	/**
	 * @param host
	 * @param path
	 */
	public HttpProxy(String host, String path) {
		this(host, path, new HashMap<String, Object>());
	}

	/**
	 * @param host
	 */
	public HttpProxy(String host) {
		this(host, "");
	}

	public HttpProxy withFiles(File[] files) {
		this.files = files;
		return this;
	}

	public HttpProxy withCookie(String cookie) {
		this.cookie = cookie;
		return this;
	}

	public HttpProxy withMethod(HttpMethod method) {
		this.method = method;
		return this;
	}

	public String getCookie() {
		return cookie;
	}

	public HttpProxy setPath(String path) {
		this.path = path;
		return this;
	}

	/**
	 * @param name
	 * @param value
	 * @return
	 */
	public HttpProxy addParameter(String name, Object value) {
		parameters.put(name, value);
		return this;
	}


	/**
	 * Sets the general request property. If a property with the key already
	 * exists, overwrite its value with the new value.
	 * <p/>
	 * <p> NOTE: HTTP requires all request properties which can
	 * legally have multiple instances with the same key
	 * to use a comma-separated list syntax which enables multiple
	 * properties to be appended into a single property.
	 *
	 * @param key   the keyword by which the request is known
	 *              (e.g., "{@code Accept}").
	 * @param value the value associated with it.
	 * @throws IllegalStateException if already connected
	 * @throws NullPointerException  if key is <CODE>null</CODE>
	 */
	public HttpProxy addRequestProperty(String key, String value) {
		if (connection == null) {
			throw new RuntimeException("call initGet or initPost first please");
		}
		connection.setRequestProperty(key, value);
		return this;
	}

	public boolean execute() {
		switch (method) {
			case GET:
				return executeGet();
			case POST:
				return executePost();
			default:
				return false;
		}
	}

	private boolean executeGet() {
		try {
			initGet();
			setupCookie();
			return get();
		} catch (IOException e) {
			fireEvent(-1, e.getLocalizedMessage());
			return false;
		}
	}

	private boolean executePost() {
		try {
			initPost();
			setupCookie();
			return post();
		} catch (IOException e) {
			fireEvent(-1, e.getLocalizedMessage());
			return false;
		}
	}

	private void setupCookie() {
		if (cookie != null) {
			addRequestProperty("Cookie", cookie);
		}
	}

	private void initGet() throws IOException {
		if (connection == null) {
			connection = (HttpURLConnection) getGETURL().openConnection();
			connection.setRequestMethod("GET");
			connection.setDoInput(true);
		}
	}

	private void initPost() throws IOException {
		if (connection == null) {
			connection = (HttpURLConnection) getPOSTURL().openConnection();
			connection.setRequestMethod("POST");
			connection.setDoInput(true);
			connection.setDoOutput(true);
		}
	}

	private URL getGETURL() throws MalformedURLException {
		String uri = host;
		if (path != null) {
			uri += path;
		}
		if (!parameters.isEmpty()) {
			uri += "?" + serializeParameters();
		}
		URL u = new URL(uri);
		return u;
	}

	private URL getPOSTURL() throws MalformedURLException {
		String uri = host;
		if (path != null) {
			uri += path;
		}
		URL u = new URL(uri);
		return u;
	}

	private String serializeParameters() {
		String serial = "";
		int count = 0;
		for (String name : parameters.keySet()) {
			String temp;
			if (count++ == 0) {
				temp = "";
			} else {
				temp = "&";
			}
			try {
				serial += String.format(temp + "%s=%s", name, URLEncoder.encode(parameters.get(name) + "", "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				serial += String.format(temp + "%s=%s", name, parameters.get(name));
			}
		}
		return serial;
	}

	private boolean get() throws IOException {
		return readResponse();
	}

	private boolean post() throws IOException {

		updatePostHeader();

		writeOutput();

		return readResponse();
	}

	private void writeOutput() throws IOException {
		DataOutputStream wr = new DataOutputStream(getOutputStream());
		sendParameters(wr);
		sendFile(wr);
	}

	private void sendParameters(DataOutputStream wr) throws IOException {
		String param = serializeParameters();
		if (files == null || files.length == 0) {
			if (param != null && !param.isEmpty()) {
				wr.writeBytes(param);
			}
		} else {
			sendParameterAsPart(wr);
		}
	}

	private void sendParameterAsPart(DataOutputStream wr) throws IOException {
		for (String name : parameters.keySet()) {
			wr.writeBytes(twoHyphens + boundary + lineEnd);
			wr.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"" + lineEnd);
			wr.writeBytes(lineEnd);
			wr.writeBytes(parameters.get(name) + "");
			wr.writeBytes(lineEnd);
		}
	}

	private void sendFile(DataOutputStream wr) throws IOException {
		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file == null) {
					continue;
				}
				wr.writeBytes(twoHyphens + boundary + lineEnd);
				wr.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
						+ file.getName() + "\"" + lineEnd);
				wr.writeBytes(lineEnd);
				int bytesRead, bytesAvailable, bufferSize;
				byte[] buffer;
				int maxBufferSize = 1024 * 1024;
				FileInputStream fileInputStream = new FileInputStream(file);
				// create a buffer of  maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read files and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {
					wr.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
				fileInputStream.close();
				// send multipart form data necesssary after files data...
				//wr.writeBytes(lineEnd);
				wr.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			}
			wr.writeBytes(lineEnd);
			wr.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			//close the streams //
			wr.flush();
			wr.close();
		}
	}

	private void updatePostHeader() {
		if (files != null) {
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("ENCTYPE", "multipart/form-data");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
		}
	}

	private boolean readResponse() throws IOException {
		int responseCode = connection.getResponseCode();
		InputStream inputStream = getInputStream(responseCode);
		//add request header
		readCookie();

		BufferedReader in = new BufferedReader(
				new InputStreamReader(inputStream));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
			response.append("\n");
		}
		in.close();
		fireEvent(responseCode, response.toString());
		connection.disconnect();
		return isRequestOk(responseCode);
	}

	private InputStream getInputStream(int responseCode) throws IOException {
		if (isRequestOk(responseCode)) {
			return getInputStream();
		}
		return getErrorInputStream();
	}

	private boolean isRequestOk(int code) {
		return code == HttpURLConnection.HTTP_OK;
	}

	private InputStream getInputStream() throws IOException {
		return connection.getInputStream();
	}

	private InputStream getErrorInputStream() throws IOException {
		return connection.getErrorStream();
	}

	private OutputStream getOutputStream() throws IOException {
		return connection.getOutputStream();
	}

	private void readCookie() {
		cookie = connection.getHeaderField("Set-Cookie");
	}

	/**
	 * @param code
	 * @param response
	 */
	private void fireEvent(int code, String response) {
		for (HttpProxyListener listener :
				listenerList) {
			listener.response(code, response);
		}
	}

	/**
	 * @param listener
	 */
	public HttpProxy addHttpProxyListener(HttpProxyListener listener) {
		listenerList.add(listener);
		return this;
	}
}
