package com.djammadev.connector;

/**
 * Created by sissoko on 14/03/2016.
 */
public interface HttpProxyListener {
    void response(int code, String content);
}
