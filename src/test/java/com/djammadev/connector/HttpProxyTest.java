package com.djammadev.connector;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by sissoko on 17/07/2016.
 */
public class HttpProxyTest {

	@Test
	public void should_send_request() {
		new HttpProxy("http://ikasugu.herokuapp.com")
				.withMethod(HttpProxy.HttpMethod.GET)
				.addParameter("test", "Test")
				.addHttpProxyListener(new HttpProxyListener() {
					@Override
					public void response(int code, String content) {
						System.out.println(code);
					}
				}).execute();
	}
}
